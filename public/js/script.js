$(document).ready(function() {
  if (localStorage.getItem('postalCode') !== null) {
    $('[data-menuanchor="pick-mailing-areas"] a').attr('href', '#pick-mailing-areas');
  }
  else {
    $('[data-menuanchor="pick-mailing-areas"] a').attr('href', 'javascript:;');
  }
  if (localStorage.getItem('category') !== null) {
    $('[data-menuanchor="enter-postal-code"] a').attr('href', '#enter-postal-code');
  }
  else {
    $('[data-menuanchor="enter-postal-code"] a').attr('href', 'javascript:;');
    $('[data-menuanchor="pick-mailing-areas"] a').attr('href', 'javascript:;');
  }

  $('#fullpage').fullpage({
    sectionsColor: ['whitesmoke', '#e73d50', '#a3d9e7', 'whitesmoke'],
    anchors: ['get-started', 'select-category', 'enter-postal-code', 'pick-mailing-areas'],
    menu: '#menu',
    easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)',
    // responsiveWidth: 1100,
    onLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {
      var leavingSlide = $(this);
      // Leaving the second slide
      if (index == 3) {
        if (localStorage.getItem('category') === null) {
          return false;
        }
      }
      // Leaving the third slide
      if (index == 4) {
        if (localStorage.getItem('postalCode') === null) {
          return false;
        }
      }
    },
  });

  $("#section1").prepend('<div class="fadeit"></div>');

  $(".tosecondPage").click(function() {
    $.fn.fullpage.moveSectionDown();
  });

  $('#section2 .category').click(function(e) {
    // Grab the text of the category card
    var category = $(this).find('span').contents().filter(function() {
      return this.nodeType == 3;
    })[0].nodeValue

    // Reset
    $('#section2 .category').each(function(e) {
      $(this).css('background-color', 'inherit');
      $(this).find(".marked").remove();
    });

    // Manage local storage and visual cues
    if (localStorage.getItem('category') === category) {
      localStorage.removeItem('category');
      $(this).css('background-color', 'inherit');
      $(this).find(".marked").remove();
      // Empty anchors
      $('[data-menuanchor="enter-postal-code"] a').attr('href', 'javascript:;');
      $('[data-menuanchor="pick-mailing-areas"] a').attr('href', 'javascript:;');
  	}
    else {
      localStorage.setItem('category', category);
  	  $(this).css('background-color', '#ff6d7e');
      $(this).prepend('<div class="marked"><i class="fa fa-check fa-2x" aria-hidden="true"></i></div>');
      // Populate anchor
      $('[data-menuanchor="enter-postal-code"] a').attr('href', '#enter-postal-code');
      if (localStorage.getItem('postalCode') !== null) {
        $('[data-menuanchor="pick-mailing-areas"] a').attr('href', '#pick-mailing-areas');
      }
    }

    $.fn.fullpage.moveSectionDown();
  });

  // Update category card background based on previous stored selections
  $('#section2 .category').each(function(e) {
    var category = $(this).find('span').contents().filter(function() {
      return this.nodeType == 3;
    })[0].nodeValue

    if (localStorage.getItem('category') === category) {
  	  $(this).css('background-color', '#ff6d7e');
      $(this).prepend('<div class="marked"><i class="fa fa-check fa-2x" aria-hidden="true"></i></div>');
  	}
  });

  $(".go").click(function() {
    var zip = document.getElementById('zip');
    if (zip.validity.valueMissing || zip.validity.patternMismatch) {
      localStorage.removeItem('postalCode');
      $(zip).addClass('invalid');
      // Empty anchor
      $('[data-menuanchor="pick-mailing-areas"] a').attr('href', 'javascript:;');
    }
    else {
      // Store zip in local storage
      localStorage.setItem('postalCode', $(zip).val());
      $(zip).removeClass('invalid');
      // Populate anchor
      $('[data-menuanchor="pick-mailing-areas"] a').attr('href', '#pick-mailing-areas');

      $.fn.fullpage.moveSectionDown();
    }
  });

  // Prepopulate zip based on previous stored selection
  if (localStorage.getItem('postalCode') !== null) {
    $('#zip').val(localStorage.getItem('postalCode'));
  }

  // Randomize background image on page load
  var bgArray = ['bg-shopping.jpg', 'bg-grocery-shopping.jpg', 'bg-thumbs-up-bags.jpg', 'bg-family-shopping.jpg', 'bg-shopping-bags.jpg', 'bg-grocery-cart.jpg'];
  var bg = bgArray[Math.floor(Math.random() * bgArray.length)];
  var path = 'img/';
  var imageUrl = path + bg;
  $('#section1').css('background-image', 'url(' + imageUrl +')');
});
