
      var map;

      require(["esri/map","esri/layers/FeatureLayer","esri/layers/LabelLayer","esri/InfoTemplate"],function(Map,FeatureLayer,LabelLayer,InfoTemplate) {
	  var initMap = function() {
                 map = new Map("map", {
                    basemap: "topo",
                    center: [-82.6389, 27.872461], // longitude, latitude
                    zoom: 10
                });
				
				map.on("load", initMapLayers);
      };
	  var initMapLayers=function(){
		  
		  var popup = new InfoTemplate();
                popup.setTitle("<div style='max-width: 200px;'><b>NTA Median Household Income</b></div>");
                popup.setContent("Demographic Value Will Go here"); 
	   var ntaFeatureLayer = new FeatureLayer("https://esri-sndbx.coxtarget.com:6443/arcgis/rest/services/AllNTAs/MapServer/0", {
                    mode: FeatureLayer.MODE_SNAPSHOT,
					infoTemplate:popup,
                    outFields: ["NTA", "FranNTA"],
                    visible: false,
                    id: "coveredNTAs"
                });
				
				var ntaLabelLayer = new LabelLayer({ id: "ntaLabels", mode: "dynamic" });
				var featureLabelRenderer ;
                ntaLabelLayer.addFeatureLayer(ntaFeatureLayer, featureLabelRenderer, "{NTA}");
                ntaLabelLayer.styling = true;
                
                
				
		var whereClause="FranNTA IN ('3261LI','3261SV','3261SW','3261LJ','3261LD','3261LF','3261SU')";
		ntaFeatureLayer.setDefinitionExpression(whereClause);
        map.addLayer(ntaFeatureLayer, 1);
		map.addLayer(ntaLabelLayer, 4);
		ntaFeatureLayer.show();
		ntaLabelLayer.show();
		ntaFeatureLayer.setOpacity(0.5)
		};
        initMap();
		
	  });
